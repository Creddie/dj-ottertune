﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HighlightableObject : MonoBehaviour
{
    [SerializeField] Material material;
    [SerializeField] string propertyName;
    [Header("Outline Settings")]
    [HideInInspector] AnimationCurve curve = new AnimationCurve();
    [SerializeField] float maxValue;
    [SerializeField] float minvalue;
    float normalizeMagnifier;
    float currentWidth;
    [SerializeField] float fadeDuration = 0.3f;
    [SerializeField] float frequency = 1f;
    float startTime;
    bool isHighlighted;
    


    public HighlightableObject()
    {
        Keyframe[] keys = { new Keyframe(0, 0, 0, 0, 0.333333f, 0.333333f), new Keyframe(1, 1, 0, 0, 0.333333f, 0.333333f) };
        curve = new AnimationCurve(keys);
        curve.postWrapMode = WrapMode.PingPong;
    }

    // Start is called before the first frame update
    void Start()
    {
        material = GetComponent<Renderer>().material;
    }

    [ContextMenu("Highlight")]
    public void Highlight()
    {
        isHighlighted = !isHighlighted;
        if (isHighlighted)
        {
            StartCoroutine(BeginHighlight());
        }
        else
        {
            StartCoroutine(EndHighlight());
        }
    }

    IEnumerator BeginHighlight()
    {

        Debug.Log("Fade in Begin");
        startTime = Time.time;
        normalizeMagnifier = 0;
        StartCoroutine(Glow());

        while ((Time.time - startTime) < fadeDuration)
        {
            normalizeMagnifier = Mathf.Lerp(0, 1, (Time.time - startTime) / fadeDuration);
            yield return null;
        }
        normalizeMagnifier = 1;
        Debug.Log("Fade in Done");
    }


    IEnumerator EndHighlight()
    {

        Debug.Log("Fade out begin");
        float endTime = Time.time;
        while ((Time.time - endTime) < fadeDuration)
        {
            normalizeMagnifier = Mathf.Lerp(1, 0, (Time.time - endTime) / fadeDuration);
            yield return null;
        }
        normalizeMagnifier = 0;

        Debug.Log("Fade out Done");
        StopCoroutine(Glow());
    }
    
    IEnumerator Glow()
    {

        Debug.Log("Fading");
        while (true)
        {
            float cValue = curve.Evaluate(Time.time * frequency);
            currentWidth = Mathf.Lerp(minvalue, maxValue, cValue);
            material.SetFloat(propertyName, currentWidth * normalizeMagnifier);
            yield return null;
        }

    }



}
