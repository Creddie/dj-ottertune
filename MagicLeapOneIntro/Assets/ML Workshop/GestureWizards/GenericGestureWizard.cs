﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.MagicLeap; // Add the MagicLeap Namespace to get access to the ML SDK APIs
using UnityEngine.Events;

public class GenericGestureWizard : MonoBehaviour
{

    private bool OKHandPose = false; // Bool used to check if the activation has happened
    private MLHandKeyPose[] gestures; // Array to store different gestures we will look for
                                      //private float colorTimer = 0f;


    [SerializeField] UnityEvent finger;
    [SerializeField] UnityEvent fist;
    [SerializeField] UnityEvent pinch;
    [SerializeField] UnityEvent thumb;
    [SerializeField] UnityEvent lPose;
    [SerializeField] UnityEvent openhand;
    [SerializeField] UnityEvent okay;
    [SerializeField] UnityEvent cPose;
    [SerializeField] UnityEvent NoPose;
    [SerializeField] UnityEvent NoHand;


    void Awake()
    {
        // Start the hand tracking API
        MLHands.Start();

        // Assign the number of gestures we will look for
        gestures = new MLHandKeyPose[10];
        gestures[0] = MLHandKeyPose.Finger; //0
        gestures[1] = MLHandKeyPose.Fist; //1
        gestures[2] = MLHandKeyPose.Pinch; //2
        gestures[3] = MLHandKeyPose.Thumb; //3
        gestures[4] = MLHandKeyPose.L; //4
        gestures[5] = MLHandKeyPose.OpenHand; //5
        gestures[6] = MLHandKeyPose.Ok; //6
        gestures[7] = MLHandKeyPose.C; //7
        gestures[8] = MLHandKeyPose.NoPose; //8
        gestures[9] = MLHandKeyPose.NoHand; //9


        //Enable to hand poses
        MLHands.KeyPoseManager.EnableKeyPoses(gestures, true, false);

        // Cube is deactivated at first
        //cube.SetActive(false);

        GameObject pointLight = new GameObject("Light");
        pointLight.AddComponent<Light>();

    }


    private void Start()
    {
        OKHandPose = true;
    }

    void OnDestroy()
    {
        // Disable hand tracking before our application ends
        MLHands.Stop();
    }

    void Update()
    {
        CheckForGestures();
    }


    void CheckForGestures()
    {
        if (OKHandPose)
        {
            // pose 0
            if (GetGesture(MLHands.Left, MLHandKeyPose.Finger))
            {
                finger.Invoke();
            }
            if (GetGesture(MLHands.Right, MLHandKeyPose.Finger))
            {
                finger.Invoke();
            }
            // pose 1
            if (GetGesture(MLHands.Left, MLHandKeyPose.Fist)
            || GetGesture(MLHands.Right, MLHandKeyPose.Fist))
            {
                fist.Invoke();
            }
            // pose 2
            if (GetGesture(MLHands.Left, MLHandKeyPose.Pinch)
            || GetGesture(MLHands.Right, MLHandKeyPose.Pinch))
            {
                pinch.Invoke();
            }
            // pose 3
            if (GetGesture(MLHands.Left, MLHandKeyPose.Thumb)
            || GetGesture(MLHands.Right, MLHandKeyPose.Thumb))
            {
                thumb.Invoke();
            }
            // pose 4
            if (GetGesture(MLHands.Left, MLHandKeyPose.L)
            || GetGesture(MLHands.Right, MLHandKeyPose.L))
            {
                lPose.Invoke();
            }
            // pose 5
            if (GetGesture(MLHands.Left, MLHandKeyPose.OpenHand)
            || GetGesture(MLHands.Right, MLHandKeyPose.OpenHand))
            {
                openhand.Invoke();
            }
            // pose 6
            if (GetGesture(MLHands.Left, MLHandKeyPose.Ok)
            || GetGesture(MLHands.Right, MLHandKeyPose.Ok))
            {
                okay.Invoke();
            }
            // pose 7
            if (GetGesture(MLHands.Left, MLHandKeyPose.C)
            || GetGesture(MLHands.Right, MLHandKeyPose.C))
            {
                cPose.Invoke();
            }
            // pose 8
            if (GetGesture(MLHands.Left, MLHandKeyPose.NoPose)
            || GetGesture(MLHands.Right, MLHandKeyPose.NoPose))
            {
                NoPose.Invoke();
            }
        }
        else
        {
            //pose 9
            if (GetGesture(MLHands.Left, MLHandKeyPose.NoHand)
            || GetGesture(MLHands.Right, MLHandKeyPose.NoHand))
            {

                NoHand.Invoke();

            }
        }
    }




    // Method used to check if a given MLHandKeyPose is recognized with over 90% confidence on a hand
    // We return true if the application recognizes the pose
    bool GetGesture(MLHand hand, MLHandKeyPose type)
    {
        if (hand != null)
        {
            if (hand.KeyPose == type)
            {
                if (hand.KeyPoseConfidence > 0.9f)
                {
                    return true;
                }
            }
        }
        return false;
    }
}
