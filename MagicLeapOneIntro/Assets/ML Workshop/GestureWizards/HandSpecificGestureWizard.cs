﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.MagicLeap; // Add the MagicLeap Namespace to get access to the ML SDK APIs
using UnityEngine.Events;

public class HandSpecificGestureWizard : MonoBehaviour
{
    [Header("Hand Selection")]
    [SerializeField] bool isLeftHand = true;

    private MLHand hand;
    
    private MLHandKeyPose[] gestures; // Array to store different gestures we will look for

    [Tooltip("Events presently are trigger every frame update")]
    [Header("Events to Trigger on Gesture")]
    [SerializeField] UnityEvent finger;
    [SerializeField] UnityEvent fist;
    [SerializeField] UnityEvent pinch;
    [SerializeField] UnityEvent thumb;
    [SerializeField] UnityEvent lPose;
    [SerializeField] UnityEvent openhand;
    [SerializeField] UnityEvent okay;
    [SerializeField] UnityEvent cPose;
    [SerializeField] UnityEvent NoPose;
    [SerializeField] UnityEvent NoHand;


    void Awake()
    {
        // Start the hand tracking API
        MLHands.Start();

        // Assign the number of gestures we will look for
        gestures = new MLHandKeyPose[10];
        gestures[0] = MLHandKeyPose.Finger; //0
        gestures[1] = MLHandKeyPose.Fist; //1
        gestures[2] = MLHandKeyPose.Pinch; //2
        gestures[3] = MLHandKeyPose.Thumb; //3
        gestures[4] = MLHandKeyPose.L; //4
        gestures[5] = MLHandKeyPose.OpenHand; //5
        gestures[6] = MLHandKeyPose.Ok; //6
        gestures[7] = MLHandKeyPose.C; //7
        gestures[8] = MLHandKeyPose.NoPose; //8
        gestures[9] = MLHandKeyPose.NoHand; //9


        //Enable to hand poses
        MLHands.KeyPoseManager.EnableKeyPoses(gestures, true, false);
        
    }


    private void Start()
    {
        if (isLeftHand)
        {
            hand = MLHands.Left;
        }
        else
        {
            hand = MLHands.Right;
        }
    }

    void OnDestroy()
    {
        // Disable hand tracking before our application ends
        MLHands.Stop();
    }

    void Update()
    {
        CheckForGestures();
    }


    void CheckForGestures()
    {
            // pose 0
            if (GetGesture(hand, MLHandKeyPose.Finger))
            {
                finger.Invoke();
            }

            // pose 1
            if (GetGesture(hand, MLHandKeyPose.Fist))
            {
                fist.Invoke();
            }
            // pose 2
            if (GetGesture(hand, MLHandKeyPose.Pinch))
            {
                pinch.Invoke();
            }
            // pose 3
            if (GetGesture(hand, MLHandKeyPose.Thumb))
            {
                thumb.Invoke();
            }

            // pose 4
            if (GetGesture(hand, MLHandKeyPose.L))
            {
                lPose.Invoke();
            }

            // pose 5
            if (GetGesture(hand, MLHandKeyPose.OpenHand))
            {
                openhand.Invoke();
            }

            // pose 6
            if (GetGesture(hand, MLHandKeyPose.Ok))
            {
                okay.Invoke();
            }

            // pose 7
            if (GetGesture(hand, MLHandKeyPose.C))
            {
                cPose.Invoke();
            }

            // pose 8
            if (GetGesture(hand, MLHandKeyPose.NoPose))
            {
                NoPose.Invoke();
            }

            //pose 9
            if (GetGesture(hand, MLHandKeyPose.NoHand))
            {

                NoHand.Invoke();

            }

    }




    // Method used to check if a given MLHandKeyPose is recognized with over 90% confidence on a hand
    // We return true if the application recognizes the pose
    bool GetGesture(MLHand hand, MLHandKeyPose type)
    {
        if (hand != null)
        {
            if (hand.KeyPose == type)
            {
                if (hand.KeyPoseConfidence > 0.9f)
                {
                    return true;
                }
            }
        }
        return false;
    }
}

