﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.MagicLeap; // Add the MagicLeap Namespace to get access to the ML SDK APIs
using UnityEngine.Events;

public class DualHandGestureWizard : MonoBehaviour
{

    private MLHandKeyPose[] gestures; // Array to store different gestures we will look for

    public GesturePairs[] pairedGestures;

    [System.Serializable]
    public class GesturePairs
    {
        public MLHandKeyPose leftPose;
        public MLHandKeyPose rightPose;
        public UnityEvent pairedGestureEvent;
        //public bool isAmbidexterous;
    }



    void Awake()
    {
        // Start the hand tracking API
        MLHands.Start();

        // Assign the number of gestures we will look for
        gestures = new MLHandKeyPose[10];
        gestures[0] = MLHandKeyPose.Finger; //0
        gestures[1] = MLHandKeyPose.Fist; //1
        gestures[2] = MLHandKeyPose.Pinch; //2
        gestures[3] = MLHandKeyPose.Thumb; //3
        gestures[4] = MLHandKeyPose.L; //4
        gestures[5] = MLHandKeyPose.OpenHand; //5
        gestures[6] = MLHandKeyPose.Ok; //6
        gestures[7] = MLHandKeyPose.C; //7
        gestures[8] = MLHandKeyPose.NoPose; //8
        gestures[9] = MLHandKeyPose.NoHand; //9


        //Enable to hand poses
        MLHands.KeyPoseManager.EnableKeyPoses(gestures, true, false);

    }


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        CheckForPairedGestures();
    }


    void CheckForPairedGestures()
    {
        foreach (GesturePairs poses in pairedGestures)
        {
            if (GetGesture(MLHands.Left, poses.leftPose)
            && GetGesture(MLHands.Right, poses.rightPose))
            {
                // cube.transform.Rotate(Vector3.up, +speed * Time.deltaTime);
                poses.pairedGestureEvent.Invoke();
            }
        }


    }



    // Method used to check if a given MLHandKeyPose is recognized with over 90% confidence on a hand
    // We return true if the application recognizes the pose
    bool GetGesture(MLHand hand, MLHandKeyPose type)
    {
        if (hand != null)
        {
            if (hand.KeyPose == type)
            {
                if (hand.KeyPoseConfidence > 0.9f)
                {
                    return true;
                }
            }
        }
        return false;
    }

}
