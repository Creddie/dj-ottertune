﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.MagicLeap; // Add the MagicLeap Namespace to get access to the ML SDK APIs
using UnityEngine.Events;

public class IndexFingerTrackerWizard : MonoBehaviour
{

    [SerializeField] GameObject movableObject;

    // Start is called before the first frame update
    void Start()
    {
        
    }


    public void moveObject()
    {
        movableObject.transform.position = FollowLeftIndexTip();
    }


    // Update is called once per frame
    Vector3 FollowLeftIndexTip()
    {
        return MLHands.Left.Index.Tip.Position;

        
    }

    void Pointlazer()
    {

    }


}
