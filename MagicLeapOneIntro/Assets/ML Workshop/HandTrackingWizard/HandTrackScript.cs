﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.MagicLeap;

public class HandTrackScript : MonoBehaviour
{
    // Stored in public pose variable. This will allow us to monitor different states of our application.
    public enum HandPoses { Ok, Finger, Thumb, OpenHand, Fist, NoPose };
    public HandPoses pose = HandPoses.NoPose;
    public Vector3[] pos; // Store the position of the three keypoints
    public GameObject sphereThumb, sphereIndex, sphereWrist; // Keypoints are linked to our three GameObjects

    private MLHandKeyPose[] _gestures; // Store the hand poses our app will recognize

    private void Start()
    {
        MLHands.Start(); // Start the hand tracking API
        _gestures = new MLHandKeyPose[5];
        _gestures[0] = MLHandKeyPose.Ok;
        _gestures[1] = MLHandKeyPose.Finger;
        _gestures[2] = MLHandKeyPose.OpenHand;
        _gestures[3] = MLHandKeyPose.Fist;
        _gestures[4] = MLHandKeyPose.Thumb;
        MLHands.KeyPoseManager.EnableKeyPoses(_gestures, true, false);

        //Define pos as an array with three elements. Note: We can currently use up to 15 keypoints.
        pos = new Vector3[3];
    }

    private void OnDestroy()
    {
        MLHands.Stop(); // Disable hand tracking before our application ends
    }

    // Check for hand poses of the left hand and change the value stored in pose accordingly.
    private void Update()
    {
        if (GetGesture(MLHands.Left, MLHandKeyPose.Ok))
        {
            pose = HandPoses.Ok;
        }
        else if (GetGesture(MLHands.Left, MLHandKeyPose.Finger))
        {
            pose = HandPoses.Finger;
        }
        else if (GetGesture(MLHands.Left, MLHandKeyPose.OpenHand))
        {
            pose = HandPoses.OpenHand;
        }
        else if (GetGesture(MLHands.Left, MLHandKeyPose.Fist))
        {
            pose = HandPoses.Fist;
        }
        else if (GetGesture(MLHands.Left, MLHandKeyPose.Thumb))
        {
            pose = HandPoses.Thumb;
        }
        else
        {
            pose = HandPoses.NoPose;
        }

        // If any hand poses are recognized, the ShowPoints method is invoked.
        if (pose != HandPoses.NoPose) ShowPoints();
    }

    private void ShowPoints()
    {
        // Left Hand Thumb tip
        pos[0] = MLHands.Left.Thumb.KeyPoints[0].Position;
        // Left Hand Index finger tip 
        pos[1] = MLHands.Left.Index.KeyPoints[0].Position;
        // Left Hand Wrist 
        pos[2] = MLHands.Left.Wrist.KeyPoints[0].Position;
        sphereThumb.transform.position = pos[0];
        sphereIndex.transform.position = pos[1];
        sphereWrist.transform.position = pos[2];
    }

    private bool GetGesture(MLHand hand, MLHandKeyPose type)
    {
        if (hand != null)
        {
            if (hand.KeyPose == type)
            {
                if (hand.KeyPoseConfidence > 0.9f)
                {
                    return true;
                }
            }
        }
        return false;
    }

}
