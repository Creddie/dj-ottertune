﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.MagicLeap; // Add the MagicLeap Namespace to get access to the ML SDK APIs
using UnityEngine.Events;

public class GestureScript : MonoBehaviour
{

    private bool OKHandPose = false; // Bool used to check if the activation has happened
    private float speed = 30.0f;  // Speed of our cube
    private float distance = 2.0f; // Distance between Main Camera and Cube
    public GameObject cube; // Reference to our Cube
    private MLHandKeyPose[] gestures; // Array to store different gestures we will look for
                                      //private float colorTimer = 0f;


    [SerializeField] UnityEvent finger;
    [SerializeField] UnityEvent fist;
    [SerializeField] UnityEvent pinch;
    [SerializeField] UnityEvent thumb;
    [SerializeField] UnityEvent LPose;
    [SerializeField] UnityEvent openhand;
    [SerializeField] UnityEvent okay;
    [SerializeField] UnityEvent CPose;
    [SerializeField] UnityEvent NoPose;
    [SerializeField] UnityEvent NoHand;


    void Awake()
    {
        // Start the hand tracking API
        MLHands.Start();

        // Assign the number of gestures we will look for
        gestures = new MLHandKeyPose[10];
        gestures[0] = MLHandKeyPose.Finger; //0
        gestures[1] = MLHandKeyPose.Fist; //1
        gestures[2] = MLHandKeyPose.Pinch; //2
        gestures[3] = MLHandKeyPose.Thumb; //3
        gestures[4] = MLHandKeyPose.L; //4
        gestures[5] = MLHandKeyPose.OpenHand; //5
        gestures[6] = MLHandKeyPose.Ok; //6
        gestures[7] = MLHandKeyPose.C; //7
        gestures[8] = MLHandKeyPose.NoPose; //8
        gestures[9] = MLHandKeyPose.NoHand; //9


        //gestures[4] = MLHandKeyPose.C;

        //Enable to hand poses
        MLHands.KeyPoseManager.EnableKeyPoses(gestures, true, false);

        // Cube is deactivated at first
        //cube.SetActive(false);

        GameObject pointLight = new GameObject("Light");
        pointLight.AddComponent<Light>();

    }

    void OnDestroy()
    {
        // Disable hand tracking before our application ends
        MLHands.Stop();
    }

    void Update()
    {
        if (OKHandPose)
        {
            if (GetGesture(MLHands.Left, MLHandKeyPose.OpenHand)
            || GetGesture(MLHands.Right, MLHandKeyPose.OpenHand))
            {
               // cube.transform.Rotate(Vector3.up, +speed * Time.deltaTime);
                openhand.Invoke();
            }
            if (GetGesture(MLHands.Left, MLHandKeyPose.Fist)
            || GetGesture(MLHands.Right, MLHandKeyPose.Fist))
            {
                //cube.transform.Rotate(Vector3.up, -speed * Time.deltaTime);
                fist.Invoke();
            }


            if (GetGesture(MLHands.Left, MLHandKeyPose.Finger))
            {
                //cube.transform.Rotate(Vector3.right, +speed * Time.deltaTime);
                finger.Invoke();
            }
            if (GetGesture(MLHands.Right, MLHandKeyPose.Finger))
            {
                //cube.transform.Rotate(Vector3.right, -speed * Time.deltaTime);
                finger.Invoke();
            }
            //if (GetGesture(MLHands.Left, MLHandKeyPose.C)
            //|| GetGesture(MLHands.Right, MLHandKeyPose.C))
            //{
            //    colorTimer += Time.deltaTime;
            //    if (colorTimer >= 2f)
            //    {
            //        cube.GetComponent<MeshRenderer>().material.color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
            //        colorTimer = 0f;
            //    }
            //}
        }
        else
        {
            if (GetGesture(MLHands.Left, MLHandKeyPose.Ok)
            || GetGesture(MLHands.Right, MLHandKeyPose.Ok))
            {
                OKHandPose = true;

                // Cube is re-activated
                //cube.SetActive(true);
                //cube.transform.position = transform.position + transform.forward * distance;
                //cube.transform.rotation = transform.rotation;
                okay.Invoke();

            }
        }
    }

    // Method used to check if a given MLHandKeyPose is recognized with over 90% confidence on a hand
    // We return true if the application recognizes the pose
    bool GetGesture(MLHand hand, MLHandKeyPose type)
    {
        if (hand != null)
        {
            if (hand.KeyPose == type)
            {
                if (hand.KeyPoseConfidence > 0.9f)
                {
                    return true;
                }
            }
        }
        return false;
    }
}