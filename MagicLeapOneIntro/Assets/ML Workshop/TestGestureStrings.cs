﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TestGestureStrings : MonoBehaviour
{
    [SerializeField] string[] strings;
    [SerializeField] TextMeshProUGUI text;

    public void SetString(int i)
    {
        text.text = strings[i];
    }
}
