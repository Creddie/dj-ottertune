﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicMovementController : MonoBehaviour
{
    public GameObject obj;
    public float speed = 1f;


    public void SetObj(GameObject go)
    {
        obj = go;
    }

    public void RotateObjUp()
    {
        obj.transform.Rotate(Vector3.up, +speed * Time.deltaTime);
    }

    public void RotateObjDown()
    {

        obj.transform.Rotate(Vector3.up, -speed * Time.deltaTime);
    }

    public void RotateObjRight()
    {
        obj.transform.Rotate(Vector3.right, +speed * Time.deltaTime);
    }


    public void RotateObjLeft()
    {
        obj.transform.Rotate(Vector3.right, -speed * Time.deltaTime);
    }


}