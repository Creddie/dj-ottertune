﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinAroundPoint : MonoBehaviour
{

    [SerializeField] Vector3 PivotPoint;
    [SerializeField] float angularSpeed = 50f;

    [ContextMenu("Reset Pivot")]
    private void ResetPivot()
    {
        PivotPoint = new Vector3(transform.position.x, transform.position.y, transform.position.z);
    }

    void OnDrawGizmos()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(PivotPoint,0.25f);
        //Draw a yello lint to the pivot point
        Gizmos.DrawLine(PivotPoint, transform.position);
        //Show Angular to Linear Velocity Vector with a blue line //will vary when not in playmode from deltatime changes
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, transform.position - (transform.forward * angularSpeed *Time.deltaTime));

    }





    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(PivotPoint,Vector3.up,angularSpeed *Time.deltaTime);
    }
}
