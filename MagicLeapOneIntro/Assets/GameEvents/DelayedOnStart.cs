﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DelayedOnStart : MonoBehaviour
{

    [SerializeField] UnityEvent thingTodo;
    [SerializeField] float delay;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ActivateAfterDelay());
    }

    IEnumerator ActivateAfterDelay()
    {
        yield return new WaitForSeconds(delay);
        thingTodo.Invoke();
    }
}
