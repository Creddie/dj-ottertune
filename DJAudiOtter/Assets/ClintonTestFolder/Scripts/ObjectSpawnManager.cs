﻿using System.Collections;
using UnityEngine;

public enum SpawnedObjectTypes
{
	Starfish
}

public class ObjectSpawnManager : MonoBehaviour
{
	#region Variables
	public static ObjectSpawnManager Instance;

	[System.Serializable]
	public struct ObstacleInfo
	{
		public float SpawnDelay;
		public SpawnedObjectTypes Type;
		public SwimmingLane Lane;
	}

	[System.Serializable]
	public struct ObstaclePrefabs
	{
		public GameObject[] Starfishes;
	}

	[Header("Settings")]
	public float ObjectDespawnTime = 5f;
	[SerializeField] private float leadTime = 30f;
	[SerializeField] private ObstaclePrefabs obstaclePrefabs;

	//[SerializeField]
	private float[] leftChannelTimes = { 0f, 32.647f, 39.706f, 46.324f, 50.294f, 53.382f, 57.353f, 74.118f, 74.559f, 75f, 81.176f, 81.618f, 82.059f, 88.235f, 89.118f,
		92.206f, 95.294f, 96.176f, 99.265f, 117.353f, 124.412f, 131.029f, 135f, 138.088f, 142.059f, 158.824f, 159.706f, 163.235f, 165.882f, 170.294f };

	//[SerializeField]
	private float[] rightChannelTimes = { 0f, 36.176f, 43.235f, 46.765f, 49.853f, 53.824f, 56.912f, 77.647f, 78.088f, 78.529f, 84.706f, 85.147f, 85.588f, 88.676f,
		91.765f, 92.647f, 95.735f, 98.824f, 99.706f, 120.882f, 127.941f, 131.471f, 134.559f, 138.529f, 141.618f, 159.265f, 162.353f, 166.765f, 169.853f };

	[Header("References")]
	public Transform SpawnPoint_Left;
	public Transform SpawnPoint_Middle;
	public Transform SpawnPoint_Right;
	#endregion

	private void Awake()
	{
		Instance = this;
		//leftChannelTimes[0] = rightChannelTimes[0] = leadTime;
		StartCoroutine(LeftSide());
		StartCoroutine(RightSide());
	}

	private IEnumerator LeftSide()
	{
		for (int i = 1; i < leftChannelTimes.Length; i++)
		{
			var val = i == 1 ? 0f : leftChannelTimes[i - 1] - leadTime;
			yield return new WaitForSeconds((leftChannelTimes[i] - leadTime) - val);
			SpawnObject(SwimmingLane.Left);
		}
	}

	private IEnumerator RightSide()
	{
		for (int j = 1; j < rightChannelTimes.Length; j++)
		{
			var val = j == 1 ? 0f : rightChannelTimes[j - 1] - leadTime;
			yield return new WaitForSeconds((rightChannelTimes[j] - leadTime) - val);
			SpawnObject(SwimmingLane.Right);
		}
	}

	private void SpawnObject(SwimmingLane lane)
	{
		foreach (var v in obstaclePrefabs.Starfishes)
		{
			if (!v.activeSelf)
			{
				///set position to proper lane spawn point
				var pos = Vector3.zero;
				switch (lane)
				{
					case SwimmingLane.Left: pos = SpawnPoint_Left.position; break;
					case SwimmingLane.Middle: pos = SpawnPoint_Middle.position; break;
					case SwimmingLane.Right: pos = SpawnPoint_Right.position; break;
				}
				v.transform.position = pos;
				v.SetActive(true);
				v.GetComponent<SpawnedObject>().Lane = lane;
				break;
			}
		}
	}
}