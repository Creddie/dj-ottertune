﻿using System.Collections;
using System;
using UnityEngine;
using TMPro;
using UnityEngine.Events;
namespace Bose.Wearable.Examples
{

    public class PairAudioTextDisplay : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI display;
        [SerializeField] AudioSource audioSource;
        [SerializeField] CanvasGroup canvasGroup;
        [SerializeField] float fadeDuration = 1f;

        [SerializeField] PairedAudioText[] instructions;
        public UnityEvent ThingToDoAtEnd;


        private void Start()
        {
            canvasGroup.alpha = 0;
            WearableControl.Instance.DeviceConnected += DisplayIntructions;
        }

        private void OnDestroy()
        {
            WearableControl.Instance.DeviceConnected -= DisplayIntructions;

        }


        [ContextMenu("Start Display")]
        public void DisplayIntructions(Device dev)
        {
            StartCoroutine(DisplaysPairedAudioText());
        }


        IEnumerator DisplaysPairedAudioText()
        {
            float startTime;
            for (int i = 0; i < instructions.Length; i++)
            {
                //set the text
                display.text = instructions[i].text;
                //if there is an audioclip, set it then play it
                if (instructions[i].audioClip != null)
                {
                    audioSource.clip = instructions[i].audioClip;
                    audioSource.Play();
                }

                ///fade in 
                startTime = Time.time;
                while ((Time.time - startTime) <= (fadeDuration))
                {
                    canvasGroup.alpha = Mathf.Lerp(0, 1, (Time.time - startTime) / fadeDuration);
                    yield return null;
                }
                canvasGroup.alpha = 1;

                // display for length of time;
                yield return new WaitForSecondsRealtime(instructions[0].GetDisplayLength());

                //fadeout
                startTime = Time.time;
                while ((Time.time - startTime) <= (fadeDuration))
                {
                    canvasGroup.alpha = Mathf.Lerp(1, 0, (Time.time - startTime) / fadeDuration);
                    yield return null;
                }
                canvasGroup.alpha = 0;
            }
            //after instructions are done, do thing, like transition to next scene
            yield return new WaitForSeconds(3f);
            ThingToDoAtEnd.Invoke();
        }

    }
}
