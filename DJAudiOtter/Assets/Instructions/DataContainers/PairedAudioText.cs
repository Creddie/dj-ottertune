﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "new PairedAudioText", menuName = "Create Paired Audio Text")]
public class PairedAudioText : ScriptableObject
{
    [TextArea(3,15)]
    public string text;
    public AudioClip audioClip;
    [Tooltip("default display time, otherwise uses audio clip length")]
    public float placeHolderLength = 7f;

    public float GetDisplayLength()
    {
        if (audioClip == null)
        {
            return placeHolderLength;
        }
        else
        {
            return audioClip.length;
        }
    }
    
}
