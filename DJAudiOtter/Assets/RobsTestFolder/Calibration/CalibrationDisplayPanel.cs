﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Bose.Wearable.Examples
{
	public class CalibrationDisplayPanel : MonoBehaviour
	{
		public static CalibrationDisplayPanel Instance;

		[Header("Scene Refs")]
		[SerializeField]
		private RotationCharacterController characterController;

		[Header("UX Refs"), Space(5)]
		[SerializeField]
		private Canvas _canvas;

		[SerializeField]
		private CanvasGroup _panelCanvasGroup;

		[SerializeField]
		private Text _labelText;

		[Header("UX Refs"), Space(5)]
		[SerializeField]
		[Range(0, float.MaxValue)]
		private float _fadeDuration = 0.66f;

		private Coroutine _fadeCoroutine;

		private void Awake()
		{
			Instance = this;
			_labelText.text = WearableConstants.WAIT_FOR_CALIBRATION_MESSAGE;

			characterController.CalibrationCompleted += OnCalibrationCompleted;
		}

		private void Start()
		{
			Show();
		}

		private void OnDestroy()
		{
			characterController.CalibrationCompleted -= OnCalibrationCompleted;

			if (_fadeCoroutine != null)
			{
				StopCoroutine(_fadeCoroutine);
				_fadeCoroutine = null;
			}
		}

		private void Show()
		{
			_panelCanvasGroup.alpha = 1f;
		}

		private void Hide()
		{
			if (_fadeCoroutine != null)
			{
				StopCoroutine(_fadeCoroutine);
			}

			_fadeCoroutine = StartCoroutine(FadePanelUI());
		}

		private void OnCalibrationCompleted()
		{
			Hide();
		}

		public void ResetPanel(bool turnOn)
		{
			var val = turnOn ? 1f : 0f;
			_panelCanvasGroup.alpha = 1f;
			_panelCanvasGroup.interactable = _panelCanvasGroup.blocksRaycasts = turnOn;
			_canvas.enabled = turnOn;
		}

		private IEnumerator FadePanelUI()
		{
			var waitForEndOfFrame = new WaitForEndOfFrame();
			var time = 0f;
			var startAlpha = _panelCanvasGroup.alpha;
			while (_panelCanvasGroup.alpha > 0f)
			{
				time += Time.unscaledDeltaTime;
				_panelCanvasGroup.alpha = startAlpha * Mathf.Clamp01(1f - (time / _fadeDuration));

				yield return waitForEndOfFrame;
			}

			ResetPanel(false);

			_fadeCoroutine = null;
		}
	}
}