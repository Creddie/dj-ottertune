﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasGroupFade : MonoBehaviour {

    CanvasGroup canvasGroup;
    [SerializeField] const float fadeWait = 10f;
    [SerializeField] float duration = 2f;


    // Use this for initialization
    void Start()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        canvasGroup.alpha = 1;
        BeginTransition(30f);
    }

    public void BeginTransition(float waitTime = fadeWait)
    {
        StopAllCoroutines();
        StartCoroutine(Fade(waitTime));

    }


    IEnumerator Fade(float wait)
    {
        canvasGroup.alpha = 1;
        yield return new WaitForSecondsRealtime(wait);

        float startTime = Time.time;
        while ((Time.time - startTime) <= (duration))
        {
            canvasGroup.alpha = Mathf.Lerp(1, 0, (Time.time - startTime) / duration);

            yield return null;
        }

        canvasGroup.alpha = 0;

    }
}
