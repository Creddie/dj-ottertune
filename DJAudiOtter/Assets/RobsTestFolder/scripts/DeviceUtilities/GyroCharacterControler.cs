﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Bose.Wearable.Examples
{
    public class GyroCharacterControler : MonoBehaviour
{

    /// <summary>
    /// The rotation matcher component attached to the rotation widget.
    /// </summary>
    [SerializeField]
    protected RotationMatcher _widgetRotationMatcher;

    /// <summary>
    /// The minimum time to wait before calibrating. Gives the user the opportunity to read and comprehend the
    /// calibration message.
    /// </summary>
    [SerializeField]
    protected float _minCalibrationTime;

    /// <summary>
    /// The maximum time to wait while calibrating.
    /// </summary>
    [SerializeField]
    protected float _maxCalibrationTime;

    /// <summary>
    /// The maximum allowable rotational velocity in degrees per second while calibrating. Waits for rotation to
    /// fall beneath this level before calibrating.
    /// </summary>
    [SerializeField]
    protected float _calibrationMotionThreshold;

    /// <summary>
    /// The amount of time to wait between target spawns in seconds.
    /// </summary>
    [SerializeField]
    protected float _spawnDelay;

    /// <summary>
    /// Invoked when calibration is complete.
    /// </summary>
    public event Action CalibrationCompleted;

    private WearableControl _wearableControl;

    private bool _calibrating;
    private float _calibrationStartTime;
    private int _lastSpawnPointIndex;
    private Quaternion _referenceRotation;

    private void Awake()
    {
        // Grab an instance of the WearableControl singleton. This is the primary access point to the wearable SDK.
        _wearableControl = WearableControl.Instance;
    }

    private void Start()
    {
        // Begin calibration immediately.
        StartCalibration();
    }

    /// <summary>
    /// Begin the calibration routine. Waits for <see cref="_minCalibrationTime"/>, then until rotational
    /// velocity falls below <see cref="_calibrationMotionThreshold"/> before sampling the rotation sensor.
    /// Will not calibrate for longer than <see cref="_maxCalibrationTime"/>.
    /// </summary>
    private void StartCalibration()
    {
        _calibrating = true;
        _calibrationStartTime = Time.unscaledTime;
    }


    private void Update()
    {
        if (_calibrating)
        {
            // While calibrating, continuously sample the gyroscope and wait for it to fall below a motion
            // threshold. When that happens, or a timeout is exceeded, grab a sample from the rotation sensor and
            //  use that as the reference rotation.
            SensorFrame frame = _wearableControl.LastSensorFrame;

            bool didWaitEnough = Time.unscaledTime > _calibrationStartTime + _minCalibrationTime;
            bool isStationary = frame.angularVelocity.value.magnitude < _calibrationMotionThreshold;
            bool didTimeout = Time.unscaledTime > _calibrationStartTime + _maxCalibrationTime;

            if ((didWaitEnough && isStationary) || didTimeout)
            {
                _referenceRotation = frame.rotationNineDof;
                _calibrating = false;

                // Pass along the reference to the rotation matcher on the widget.
                _widgetRotationMatcher.SetRelativeReference(frame.rotationNineDof);

                if (CalibrationCompleted != null)
                {
                    CalibrationCompleted.Invoke();
                }

                // Spawn the first target after calibration completes.
                //Invoke("SpawnTarget", _spawnDelay);
            }
        }
    }
}
}
