﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChanger : MonoBehaviour
{
    Color[] startColor = new Color[2];
    [SerializeField] Color[] newColor;
    [SerializeField] GameObject[] things;
    bool isDefaultColor = true;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < things.Length; i++)
        {
            startColor[i] = things[i].GetComponent<Renderer>().material.GetColor("_Color");
        }
    }

    // Update is called once per frame
    public void ChangeColor()
    {


        for (int i = 0; i < things.Length; i++)
        {
            if (isDefaultColor)
            {
                things[i].GetComponent<Renderer>().material.SetColor("_Color", newColor[i]);
            }
            else
            {
                things[i].GetComponent<Renderer>().material.SetColor("_Color", startColor[i]);
            }
        }
        isDefaultColor = !isDefaultColor;
    }

    
    public void resetColor()
    {


        for (int i = 0; i < things.Length; i++)
        {

                things[i].GetComponent<Renderer>().material.SetColor("_Color", startColor[i]);
        }
        isDefaultColor = true;
    }

    public void GoGreen()
    {


        for (int i = 0; i < things.Length; i++)
        {

            things[i].GetComponent<Renderer>().material.SetColor("_Color", Color.green);

        }
        isDefaultColor = false;
    }


    public void GoRed()
    {


        for (int i = 0; i < things.Length; i++)
        {
                things[i].GetComponent<Renderer>().material.SetColor("_Color", Color.red);
        }
        isDefaultColor = false;
    }


}
