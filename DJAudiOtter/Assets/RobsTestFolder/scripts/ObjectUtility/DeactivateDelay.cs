﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivateDelay : MonoBehaviour
{
    
    void OnEnable()
    {
        StartCoroutine(DeactivateAfterDelay());
    }

    
    IEnumerator DeactivateAfterDelay()
    {
        yield return new WaitForSeconds(4f);
        gameObject.SetActive(false);
    }
}
