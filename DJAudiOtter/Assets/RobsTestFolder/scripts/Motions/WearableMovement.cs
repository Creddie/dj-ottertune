﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using Bose.Wearable;

public class WearableMovement : MonoBehaviour
{
    [SerializeField] float enableMovementDelay = 1f;
    [SerializeField] float threshold = 0.2f;
    Vector3 acceleration;
    bool canMove = true;

    [Header("Calbration Settings")]
    [SerializeField] float _minCalibrationTime = 5;
    [SerializeField] float _maxCalibrationTime = 10;
    [SerializeField] float _calibrationMotionThreshold= 1; 
    Vector3 accelerationCalibartion = Vector3.zero;


    private WearableControl _wearableControl;

    public event Action CalibrationCompleted;


    //60b 1b
    //johns music  136 minute consistantly

    private void Awake()
    {
        // Grab an instance of the WearableControl singleton. This is the primary access point to the wearable SDK.
        _wearableControl = WearableControl.Instance;
    }


    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Calbrating());
    }

    private void Update()
    {
        ReadSensor();
    }

    void ReadSensor()
    {
        foreach (SensorFrame frame in WearableControl.Instance.CurrentSensorFrames)
        {
            acceleration = frame.acceleration.value - accelerationCalibartion; ;
            Debug.Log(acceleration);
        }
        MovePlayer();
    }


    void MovePlayer()
    {
        

        if (canMove)
        {
            if (Mathf.Abs(acceleration.x) > threshold)
            {
                canMove = false;
                if (acceleration.x < 0f) {
                    transform.position += Vector3.right * threshold * Time.deltaTime;
                } else
                {
                    transform.position += Vector3.left * threshold * Time.deltaTime;
                }
                StartCoroutine(EnableMovementAfterDelay());
            }
        }

        if (Mathf.Abs(acceleration.z) > threshold)
        {
            canMove = false;
            if (acceleration.x < 0f)
            {
                transform.position += Vector3.forward * threshold * Time.deltaTime;
            }
            else
            {
                transform.position += Vector3.back * threshold * Time.deltaTime;
            }
            StartCoroutine(EnableMovementAfterDelay());
        }
    }



    IEnumerator EnableMovementAfterDelay()
    {
        yield return new WaitForSecondsRealtime(enableMovementDelay);
        canMove = true;
    }

    IEnumerator Calbrating()
    {

        float _calibrationStartTime = Time.time;
        bool didWaitEnough = false;
        bool isStationary = false;
        bool didTimeout = false;
        WearableControl _wearableControl = WearableControl.Instance;
        SensorFrame frame = _wearableControl.LastSensorFrame;

        // While calibrating, continuously sample the gyroscope and wait for it to fall below a motion
        // threshold. When that happens, or a timeout is exceeded, grab a sample from the rotation sensor and
        //  use that as the reference rotation.
        while ((didWaitEnough && isStationary) || didTimeout)
        {

            frame = _wearableControl.LastSensorFrame;


            didWaitEnough = (Time.time - _calibrationStartTime) > _minCalibrationTime;
            isStationary = frame.angularVelocity.value.magnitude < _calibrationMotionThreshold;
            didTimeout = (Time.time - _calibrationStartTime) > _maxCalibrationTime;
            yield return null;
        }
        if ((didWaitEnough && isStationary) || didTimeout) // this advances in sucessful calibration
        {

            accelerationCalibartion = frame.acceleration;

            if (CalibrationCompleted != null)
            {
                CalibrationCompleted.Invoke();
            }

            Debug.Log("Calibration Succeeded");
            // After calibration completes, start the level
            //    <---level start goes here
        }
        else // if calibration failed
        {

            Debug.Log("Calibration Failed");

        }


    }
}

