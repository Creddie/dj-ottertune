﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtterSenseController : MonoBehaviour
{
    [SerializeField] Transform followTransform;
    [SerializeField] AnimationCurve normalCurve = new AnimationCurve();
    [SerializeField] float maxWaggleDegrees = 16f;
    [SerializeField] float maxWaggleSize = .1f;
    [SerializeField] float frequency = 1f;
    Vector3 startScale;

    public OtterSenseController()
    {
        normalCurve.AddKey(0f, -1f);
        normalCurve.AddKey(.5f, 1f);
        normalCurve.AddKey(1f, -1f);
        normalCurve.postWrapMode = WrapMode.PingPong;

    }

    private void Start()
    {
        startScale = transform.localScale;
    }

    Vector3 CurvesToVector3()
    {
        float y = normalCurve.Evaluate(Time.time * frequency) * maxWaggleDegrees;
        return new Vector3(0, y, 0);
    }

    public void CurveToLocalRotaion()
    {
        transform.localRotation = Quaternion.Euler(CurvesToVector3());
    }

    private void Update()
    {
        CurveToLocalRotaion();
        transform.localScale = startScale + (startScale * (maxWaggleSize* normalCurve.Evaluate(Time.time * frequency* 2)));
        transform.position = followTransform.position;
    }
}
