﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Bose.Wearable.Extensions;

namespace Bose.Wearable.Extensions{
public class LocalRotationWrapper : MonoBehaviour
{
    Quaternion startRotation;
    [SerializeField] Transform rotationSource;
    [SerializeField] Transform targetTransform;
   // Quaternion targetStartRot;
    [SerializeField] Transform bodyTransform;
   // Quaternion bodyStartRot;
    [SerializeField] bool useBody;
    [Range(0f, 1f)]
    [SerializeField] float damping = .0006f;

    [SerializeField] private Vector3 MinHeadRotation;
    [SerializeField] private Vector3 MaxHeadRotation;

    private void Start()
    {
        if (rotationSource == null)
        {
            rotationSource = FindObjectOfType<RotationMatcher>().transform;
        }
        //targetStartRot = targetTransform.localRotation;
       //bodyStartRot = bodyTransform.localRotation;
    }

    private void Update()
    {
        targetTransform.localRotation = /*targetStartRot */ rotationSource.localRotation;
        if (useBody)
        {
            bodyTransform.localRotation = /*bodyStartRot */ Quaternion.Euler(damping * rotationSource.localRotation.eulerAngles);
        }

		//targetTransform.localRotation = ClampedRotation();
    }

		//private Quaternion ClampedRotation()
		//{
		//	var targetRotation = /*targetStartRot */ rotationSource.localRotation;

		//	if (targetRotation.x < MinHeadRotation.x) targetRotation.x = MinHeadRotation.x;
		//	else if (targetRotation.x > MaxHeadRotation.x) targetRotation.x = MaxHeadRotation.x;
		//	if (targetRotation.y < MinHeadRotation.y) targetRotation.y = MinHeadRotation.y;
		//	else if (targetRotation.y > MaxHeadRotation.y) targetRotation.y = MaxHeadRotation.y;
		//	if (targetRotation.z < MinHeadRotation.z) targetRotation.z = MinHeadRotation.z;
		//	else if (targetRotation.z > MaxHeadRotation.z) targetRotation.z = MaxHeadRotation.z;

		//	//var newX = Mathf.Max(targetRotation.x, MinHeadRotation.x);
		//	//newX = Mathf.Min(newX, MaxHeadRotation.x);
		//	//var newY = Mathf.Max(targetRotation.y, MinHeadRotation.y);
		//	//newY = Mathf.Min(newY, MaxHeadRotation.y);
		//	//var newZ = Mathf.Max(targetRotation.z, MinHeadRotation.z);
		//	//newZ = Mathf.Min(newY, MaxHeadRotation.z);
		//	//targetRotation = new Quaternion(newX, newY, newZ, targetRotation.w);
		//	return targetRotation;
		//}
	} 
}