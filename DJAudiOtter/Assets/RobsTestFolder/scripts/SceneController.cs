﻿using System.Collections;
using System.Collections.Generic;
using Bose.Wearable;
using Bose.Wearable.Examples;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
	public static SceneController Instance;

	[SerializeField] private CanvasGroup fadeComp;
	[SerializeField] private float fadeTime = 1f;
	[SerializeField] private float initialFadeDelay = 2f;
	private float timer;

	private void Awake()
	{
		Instance = this;
		SaveLoadManager.Init();
		fadeComp.alpha = 1f;
		WearableControl.Instance.DeviceConnected += StartGame;
		///root scene loads title scene immediately
		NextScene();
	}

	private void StartGame(Device obj)
	{
		//Debug.Log("connected");
		Player.ReadyToStart = true;
		RotationCharacterController.Instance.StartCalibration();
		StartCoroutine(FadingScreen(false, initialFadeDelay));
	}

	private void NextScene()
    {
		var sceneName = SceneManager.GetActiveScene().name;
		//if (sceneName.Contains("Main")) return;
		var index = sceneName.Contains("Title") ? 2 : 1;
		SceneManager.LoadScene(index);
    }

	/// <summary>
	/// called by Bose AR gesture, referenced in inspector
	/// </summary>
	public void StartLevelTransition()
	{
		StartCoroutine(FadingScreen(true));
	}

	private IEnumerator FadingScreen(bool fadeToBlack, float delay = 0f)
	{
		yield return new WaitForSeconds(delay);
		var start = fadeComp.alpha;
		var end = fadeToBlack ? 1f : 0f;
		while(timer < fadeTime)
		{
			timer += Time.deltaTime;
			fadeComp.alpha = Mathf.Lerp(start, end, timer / fadeTime);
			yield return new WaitForEndOfFrame();
		}
		timer = 0f;
		fadeComp.alpha = end;
		if (fadeToBlack)
		{
			NextScene();
			StartCoroutine(FadingScreen(false));
		}
	}
}