﻿namespace Bose.Wearable
{
	internal enum LogLevel
	{
		Release = 0,
		Debug = 1
	}
}
