﻿using System.Collections;
using Bose.Wearable;
using UnityEngine;

public enum SwimmingLane
{
	Left,
	Middle,
	Right
}

[RequireComponent(typeof(AudioSource))]
public class Player : MonoBehaviour
{
	#region Variables
	public static Player Instance;
	public static bool ReadyToStart;
	public static int CurrentCombo;
	public static int CurrentHighCombo;

	[SerializeField] private float maxMovementDelta = 1f;
	[SerializeField] private float movementMultiplier = 1f;
	[SerializeField] private Transform startingPosition;

	[HideInInspector] public SwimmingLane CurrentLane;
	private AudioSource aSource;
	//private float referenceAngle;
	private Transform rotationMatcher;
	#endregion

	private void Awake()
	{
		Instance = this;
		CurrentCombo = 0;
		CurrentHighCombo = 0;
		CurrentLane = SwimmingLane.Middle;
		aSource = GetComponent<AudioSource>();
		rotationMatcher = PlayerAvatar.Instance.RotationMatcher;
		//referenceAngle = -rotationMatcher.rotation.y;

		var avatar = PlayerAvatar.Instance.transform;
		avatar.SetParent(transform);
		avatar.position = startingPosition.position;
		avatar.rotation = startingPosition.rotation;
		avatar.localScale = startingPosition.localScale;
	}

	private void Update()
	{
		MovePlayerAvatar();
		CheckSwimmingLane();
	}

	private void MovePlayerAvatar()
	{
		var cameraAngle = rotationMatcher.rotation.z;// + referenceAngle;
		var oldPos = transform.localPosition;
		var val = cameraAngle * movementMultiplier;
		if (val < -maxMovementDelta) val = -maxMovementDelta;
		else if (val > maxMovementDelta) val = maxMovementDelta;
		var pos = new Vector3(-val, oldPos.y, oldPos.z);
		transform.localPosition = pos;
	}

	private void CheckSwimmingLane()
	{
		var val = transform.localPosition.x;
		var ratio = TilingEnvironment.Instance.TileWidth / 6;
		if (val < -ratio) CurrentLane = SwimmingLane.Left;
		else if (val > ratio) CurrentLane = SwimmingLane.Right;
		else CurrentLane = SwimmingLane.Middle;
	}

	private void OnTriggerEnter(Collider other)
	{
		var comp = other.GetComponent<SpawnedObject>();
		if (null != comp && CurrentLane == comp.Lane) ResolveTrigger(comp);
	}

	private void OnTriggerStay(Collider other)
	{
		var comp = other.GetComponent<SpawnedObject>();
		if (null != comp && CurrentLane == comp.Lane) ResolveTrigger(comp);
	}

	private void ResolveTrigger(SpawnedObject other)
	{
		other.TakenByPlayer = true;
		aSource.PlayOneShot(MusicManager.Instance.PickupSound);
		other.gameObject.SetActive(false);
		CurrentCombo++;
		ScoreCard.Instance.AddPoints();
		ScoreCard.Instance.SetMultiplier(CurrentCombo + 1);
		if (CurrentCombo > CurrentHighCombo) CurrentHighCombo = CurrentCombo;
	}

	public void TallyScores()
	{
		if(ScoreCard.Instance.Score > SaveLoadManager.Highscore)
		{
			var data = new SaveDataCSV("Highscore", ScoreCard.Instance.Score);
			SaveLoadManager.SaveData(data);
		}
		if(CurrentHighCombo > SaveLoadManager.HighCombo)
		{
			var data = new SaveDataCSV("HighCombo", CurrentHighCombo);
			SaveLoadManager.SaveData(data);
		}
	}
}