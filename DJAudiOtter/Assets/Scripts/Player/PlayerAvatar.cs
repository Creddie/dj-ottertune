﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAvatar : MonoBehaviour
{
	public static PlayerAvatar Instance;
	public Transform RotationMatcher;

	private void Awake()
	{
		Instance = this;
	}
}