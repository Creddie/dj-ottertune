﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreCard : MonoBehaviour
{
	public static ScoreCard Instance;

    [Header("Score Holder")]
    [SerializeField]
    TextMeshProUGUI ScoreText;
    [Header("Bonus Holder")]
    [SerializeField] TextMeshProUGUI BonusText;
    [SerializeField] CanvasGroup bonusCanvasGroup;
    [SerializeField] float bonusYTravel = 30f;
    Vector3 bonusStartPos;
    Vector3 bonusEndPos;
    [SerializeField] float bonusDuration = 1.6f;

    [Header("Multiplier Holder")]
    [SerializeField] TextMeshProUGUI MultiplierText;
    Vector3 multiplierStartPos;
    [HideInInspector] AnimationCurve MuliplierCurve = new AnimationCurve();
    [SerializeField] float frequencyY = 0.441227f;
    [SerializeField] float frequencyZ = 0.441227f;
    [SerializeField] float movementScaleY = 20f;
    [SerializeField] float movementScaleZ = 10f;

    public ScoreCard()
    {
        MuliplierCurve.AddKey(0f, 0f);
        MuliplierCurve.AddKey(.5f, 1f);
        MuliplierCurve.AddKey(1f, 0f);
        MuliplierCurve.postWrapMode = WrapMode.Loop;
    }

    [HideInInspector] public int Score;
    private int multiplier = 1;
    private int tempScore;

    private void Start()
    {
		Instance = this;
        bonusStartPos = BonusText.transform.localPosition;
        bonusEndPos = new Vector3(bonusStartPos.x, bonusStartPos.y + bonusYTravel, bonusStartPos.z);
        bonusCanvasGroup.alpha = 0;
        multiplierStartPos = MultiplierText.transform.localPosition;
		ScoreText.SetText("0xp");
        StartCoroutine(DisplayedBonus());
    }

    [SerializeField]
    private float counterDuration = 0.2f;

    //pass point value, or use default value
    public void AddPoints(int xp = 10)
    {
        tempScore = Score;
		float addedPoints = xp * multiplier;
        Score += (int)addedPoints;
		BonusText.text = "+" + addedPoints + "xp";
        StopCoroutine("adjustDisplayedScore");
        StartCoroutine("adjustDisplayedScore", addedPoints);
        StopCoroutine(FadeBonus());
        StartCoroutine(FadeBonus());
    }

    public void SetMultiplier(int i = 1)
    {
        multiplier = i;
        MultiplierText.text = multiplier + "x";
    }

    IEnumerator adjustDisplayedScore(float newPoints)
    {
        float startTime = Time.time;
        float scrollingScore = 0;

        while ((Time.time - startTime) < counterDuration)
        {

            scrollingScore = Mathf.Lerp(tempScore, Score, (Time.time - startTime) / counterDuration);

            ScoreText.text = (int)scrollingScore + "xp";
            yield return null;
        }

        ScoreText.SetText(Score + "xp");
    }

    IEnumerator DisplayedBonus()
    {
        float startTime = Time.time;

        while (true)
        {
            float x = multiplierStartPos.x + ((-0.5f + MuliplierCurve.Evaluate(Time.time * frequencyZ * multiplier)) * movementScaleZ);
            float y = multiplierStartPos.y + ((-0.5f + MuliplierCurve.Evaluate(Time.time * frequencyY * multiplier)) * movementScaleY);
            float z = multiplierStartPos.z;

            MultiplierText.transform.localPosition = new Vector3(x, y, z);
            yield return null;
        }
    }

    IEnumerator FadeBonus()
    {
        BonusText.transform.position = bonusStartPos;
        bonusCanvasGroup.alpha = 1;

        float startTime = Time.time;
        while ((Time.time - startTime) < (bonusDuration))
        {
            bonusCanvasGroup.alpha = Mathf.Lerp(1, 0, (Time.time - startTime) / bonusDuration);
            BonusText.transform.localPosition = Vector3.Lerp(bonusStartPos, bonusEndPos, (Time.time - startTime) / bonusDuration);
            yield return null;
        }

        bonusCanvasGroup.alpha = 0;
    }
}