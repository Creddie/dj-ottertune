﻿public class SaveDataCSV
{
	public static string HeaderRow()
	{
		return "Key," +
				"Value\n";
	}

	public override string ToString()
	{
		return Key + "," +
				Value + "\n";
	}

	public readonly string Key;
	public readonly string Value;

	public SaveDataCSV(string key, int value)
	{
		Key = key;
		Value = value.ToString();
	}
}