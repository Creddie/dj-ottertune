﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SaveLoadManager
{
	private static readonly string fileName = Application.persistentDataPath + "/data_highscores.csv";
	private static string textString;
	private static string[] entries;
	public static Dictionary<string, string> DataCollection = new Dictionary<string, string>();
	public static int Highscore;
	public static int HighCombo;

	public static void Init()
	{
		if (!File.Exists(fileName)) InitFile();
		UpdateTextString();
	}

	private static void InitFile()
	{
		File.WriteAllText(fileName, SaveDataCSV.HeaderRow());
		SaveDataCSV data = null;
		data = new SaveDataCSV("Highscore", 0);
		File.AppendAllText(fileName, data.ToString());
		data = new SaveDataCSV("HighCombo", 0);
		File.AppendAllText(fileName, data.ToString());
	}

	private static void UpdateTextString()
	{
		textString = File.ReadAllText(fileName);
		entries = textString.Split(new char[] { '\n' });
		LoadData();
	}

	private static void LoadData()
	{
		DataCollection.Clear();
		for (int i = 1; i < entries.Length - 1; i++)
		{
			var entryData = entries[i].Split(new char[] { ',' });
			DataCollection.Add(entryData[0], entryData[1]);
			var val = 0;
			int.TryParse(entryData[1], out val);
			if (i == 1) Highscore = val;
			else HighCombo = val;
		}
	}

	public static void SaveData(SaveDataCSV data)
	{
		for (int i = 0; i < entries.Length - 1; i++)
		{
			var entryData = entries[i].Split(new char[] { ',' });
			var key = entryData[0];
			if (key == data.Key)
			{
				var currentValueInt = 0;
				int.TryParse(entryData[1], out currentValueInt);
				textString = textString.Replace(key + "," + currentValueInt, key + "," + data.Value.ToString());
				File.WriteAllText(fileName, textString);
				UpdateTextString();
				return;
			}
		}
		File.AppendAllText(fileName, data.ToString());
		UpdateTextString();
	}
}