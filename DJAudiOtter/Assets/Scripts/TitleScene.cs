﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TitleScene : MonoBehaviour
{
	[SerializeField] private Transform playerStartingPosition;
	[SerializeField] private TextMeshProUGUI scoreTextComponent;

	private void Start()
	{
		Debug.Log(name);
		var avatar = PlayerAvatar.Instance.transform;
		avatar.position = playerStartingPosition.position;
		avatar.rotation = playerStartingPosition.rotation;
		avatar.localScale = playerStartingPosition.localScale;
		scoreTextComponent.SetText("High Score = " + SaveLoadManager.Highscore.ToString() + "\n" + "High Combo = " + SaveLoadManager.HighCombo.ToString());
	}
}