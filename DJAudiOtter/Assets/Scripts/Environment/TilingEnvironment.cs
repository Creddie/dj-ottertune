﻿using System.Collections.Generic;
using UnityEngine;

public class TilingEnvironment : MonoBehaviour
{
	public static TilingEnvironment Instance;

	[Header("Tiling")]
	public float TileWidth;
	[SerializeField] private float tileMovementSpeed = 4f;
	[SerializeField] private float tileSpawnRate = 1f;
	[SerializeField]  private float tileLength = 23f;
	[SerializeField] private GameObject[] tiles;
	private LinkedList<GameObject> activeTiles = new LinkedList<GameObject>();
	public float WaterTileMovementSpeed = 4f;
	[SerializeField] private float waterTileSpawnRate = 4.5f;
	[SerializeField] private float waterTileLength = 25f;
	[SerializeField] private GameObject[] waterTiles;
	private LinkedList<GameObject> activeWaterTiles = new LinkedList<GameObject>();
	private float timer;
	private float waterTimer;

	private void Awake()
	{
		Instance = this;
		activeTiles.AddFirst(tiles[0]);
		activeTiles.AddLast(tiles[1]);
		activeTiles.AddLast(tiles[2]);
		activeWaterTiles.AddFirst(waterTiles[0]);
		activeWaterTiles.AddLast(waterTiles[1]);
		activeWaterTiles.AddLast(waterTiles[2]);
	}

	private void Update()
	{
		if (!Player.ReadyToStart) return;
		MoveActiveTiles();
		CheckTileSpawning();
	}

	private void MoveActiveTiles()
	{
		foreach (var v in activeTiles)
			v.transform.Translate(0f, 0f, -tileMovementSpeed * Time.deltaTime, Space.Self);
		foreach (var v in activeWaterTiles)
			v.transform.Translate(0f, 0f, -WaterTileMovementSpeed * Time.deltaTime, Space.Self);
	}

	private void CheckTileSpawning()
	{
		timer += Time.deltaTime;
		waterTimer += Time.deltaTime;
		if (timer >= tileSpawnRate)
		{
			timer = 0f;
			var oldTile = activeTiles.First.Value;
			activeTiles.RemoveFirst();
			var obj = GetNextTile(false);
			var nearPos = activeTiles.Last.Value.transform.position;
			activeTiles.AddLast(obj);
			obj.transform.position = new Vector3(nearPos.x, nearPos.y, nearPos.z + tileLength);
			obj.SetActive(true);
			oldTile.SetActive(false);
		}
		if (waterTimer >= waterTileSpawnRate)
		{
			waterTimer = 0f;
			var oldTile = activeWaterTiles.First.Value;
			activeWaterTiles.RemoveFirst();
			var obj = GetNextTile(true);
			var nearPos = activeWaterTiles.Last.Value.transform.position;
			activeWaterTiles.AddLast(obj);
			obj.transform.position = new Vector3(nearPos.x, nearPos.y, nearPos.z + waterTileLength);
			obj.SetActive(true);
			oldTile.SetActive(false);
		}
	}

	private GameObject GetNextTile(bool waterTile)
	{
		///get all inactive tiles
		var possibilities = new List<GameObject>();
		var list = waterTile ? waterTiles : tiles;
		foreach(var v in list)
			if (!v.activeSelf)
				possibilities.Add(v);
		///choose random inactive tile
		var rando = Random.Range(0, possibilities.Count);
		return possibilities[rando];
	}
}