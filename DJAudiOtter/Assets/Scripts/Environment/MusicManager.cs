﻿using System.Collections;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
	public static MusicManager Instance;
	private float songLength;
	public AudioClip PickupSound;
	[SerializeField] private float endLevelWaitTime = 2f;

	private void Awake()
	{
		Instance = this;
		StartCoroutine(PlayingMusic());
	}

	private IEnumerator PlayingMusic()
	{
		yield return new WaitForSeconds(GetComponentInChildren<AudioSource>().clip.length);
		Player.Instance.TallyScores();
		yield return new WaitForSeconds(endLevelWaitTime);
		PlayerAvatar.Instance.transform.SetParent(null);
		DontDestroyOnLoad(PlayerAvatar.Instance.gameObject);
		SceneController.Instance.StartLevelTransition();
	}
}