﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SpawnedObject : MonoBehaviour
{
	[HideInInspector] public SpawnedObjectTypes Type;
	[HideInInspector] public SwimmingLane Lane;
	private float movementMultiplier;
	private float timer;
	private AudioSource aSource;
	[HideInInspector] public bool TakenByPlayer;

	private void Awake()
	{
		aSource = GetComponent<AudioSource>();
		switch(Type)
		{
			case SpawnedObjectTypes.Starfish: movementMultiplier = 1f; break;
			default: break;
		}
		movementMultiplier *= TilingEnvironment.Instance.WaterTileMovementSpeed;
	}

	private void Update()
	{
		timer += Time.deltaTime;
		if(timer >= ObjectSpawnManager.Instance.ObjectDespawnTime)
		{
			gameObject.SetActive(false);
			return;
		}
		transform.Translate(0f, 0f, movementMultiplier * Time.deltaTime, Space.Self);
	}

	private void OnDisable()
	{
		timer = 0f;
		TakenByPlayer = false;
	}

	private void OnTriggerEnter(Collider other)
	{
		var comp = other.GetComponent<MissRewardTrigger>();
		if (null != comp)
		{
			Player.CurrentCombo = 0;
			ScoreCard.Instance.SetMultiplier(1);
		}
	}
}