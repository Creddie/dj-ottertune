﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class ScriptedBone
{
    [HideInInspector] public string name;
    public Transform target;
    Vector3 startRotation;
    [HideInInspector] public AnimationCurve normalCurve = new AnimationCurve();

    [Range(0.1f, 2f)] public float frequencyX = 1f;
    [Range(0.1f, 2f)] public float frequencyY = 1f;
    [Range(0.1f, 2f)] public float frequencyZ = 1f;




    public Vector3 rotationScales;

    public ScriptedBone()
    {
        normalCurve.AddKey(0f, 0f);
        normalCurve.AddKey(.5f, 1f);
        normalCurve.AddKey(1f, 0f);
        normalCurve.postWrapMode = WrapMode.Loop;
        rotationScales = new Vector3(0, 30, 45);
    }

    public void SetStartPos()
    {
        startRotation = target.transform.localRotation.eulerAngles;
    }

    Vector3 CurvesToVector3()
    {
        //offset by .5 to make the midpoint the start Position
        float x = startRotation.x + ((-0.5f + normalCurve.Evaluate(Time.time * frequencyX)) * rotationScales.x);
        float y = startRotation.y + ((-0.5f + normalCurve.Evaluate(Time.time * frequencyY)) * rotationScales.y);
        float z = startRotation.z + ((-0.5f + normalCurve.Evaluate(Time.time * frequencyZ)) * rotationScales.z);

        return new Vector3(x, y, z);
    }

    public void CurveToLocalRotaion()
    {
        target.transform.localRotation = Quaternion.Euler(CurvesToVector3());
    }
}
