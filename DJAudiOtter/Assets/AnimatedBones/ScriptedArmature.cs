﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptedArmature : MonoBehaviour
{
    public ScriptedBone[] scriptedBones = new ScriptedBone[1];

    private void Start()
    {
        foreach (ScriptedBone bone in scriptedBones)
        {
            bone.SetStartPos();
        }
    }

    // Update is called once per frame
    void Update()
    {
        foreach (ScriptedBone bone in scriptedBones)
        {
            bone.CurveToLocalRotaion();
        }
    }

    public void OnValidate()
    {
        foreach (ScriptedBone bone in scriptedBones)
        {
            bone.name = bone.target.gameObject.name;
        }
    }


}
