﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicTimings : MonoBehaviour
{
    [SerializeField] List<GameObject> starfishes = new List<GameObject>();
    [Header("SpawnPoints")]
    [SerializeField] Transform leftSpawn;
    [SerializeField] Transform rightSpawn;
    const float leadTime = 30f;

    [SerializeField]
	float[] LeftChannel = { leadTime, 32.647f, 39.706f, 46.324f, 50.294f, 53.382f, 57.353f, 74.118f, 74.559f, 75f, 81.176f, 81.618f, 82.059f, 88.235f, 89.118f,
		92.206f, 95.294f, 96.176f, 99.265f, 117.353f, 124.412f, 131.029f, 135f, 138.088f, 142.059f, 158.824f, 159.706f, 163.235f, 165.882f, 170.294f };

    [SerializeField]
	float[] RightChannel = { leadTime, 36.176f, 43.235f, 46.765f, 49.853f, 53.824f, 56.912f, 77.647f, 78.088f, 78.529f, 84.706f, 85.147f, 85.588f, 88.676f,
		91.765f, 92.647f, 95.735f, 98.824f, 99.706f, 120.882f, 127.941f, 131.471f, 134.559f, 138.529f, 141.618f, 159.265f, 162.353f, 166.765f, 169.853f };

    void Start()
    {
        StartCoroutine(LeftSide());
        StartCoroutine(RightSide());
    }

    IEnumerator LeftSide()
    {
        for (int i = 1; i < LeftChannel.Length; i++)
        {
            yield return new WaitForSeconds(LeftChannel[i] - LeftChannel[i - 1]);
            SpawnStarfish(leftSpawn.position);
        }
    }

    IEnumerator RightSide()
    {
        for (int j = 1; j < RightChannel.Length; j++)
        {
            yield return new WaitForSeconds(RightChannel[j] - RightChannel[j - 1]);
            SpawnStarfish(rightSpawn.position);
        }
    }

    void SpawnStarfish(Vector3 newPos)
    {
        for(int i = 0; i < starfishes.Count; i++)
        {
            if (starfishes[i].activeSelf == false)
            {
                starfishes[i].SetActive(true);
                starfishes[i].transform.position = newPos;
            }
        }
    }
}