﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*this script moves in local sapce, attach this script to the AudioSource,
 make it a child of another GameObject and move that object to different lanes as needed 
 */
[RequireComponent(typeof(AudioSource))]
public class GoldenPath : MonoBehaviour
{
    public AnimationCurve curve = new AnimationCurve();
    [Range(0f, 10f)]
    public float magnifier = 1f;
    [Range(0f, 3f)]
    public float frequency = 1f;

    public GoldenPath() {
        curve.AddKey(0f, 0f);
        curve.AddKey(.5f, 1f);
        curve.AddKey(1f, 0f);
        curve.postWrapMode = WrapMode.Loop;
    }


    // Update is called once per frame
    void Update()
    {
        //Offset to put center at center of start
        float newX = -0.5f + curve.Evaluate(Time.time * frequency);
        Vector3 newPos = new Vector3(newX, 0, 0) * magnifier;
        transform.localPosition = newPos;
    }


    public void SetMagnifier(float mag)
    {
        magnifier = mag;
    }


}
